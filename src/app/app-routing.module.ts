import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListarComponent} from './Usuario/listar/listar.component';
import { AddComponent} from './Usuario/add/add.component';

const routes: Routes = [
	{path:'listar',component:ListarComponent},
	{path:'add',component:AddComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
