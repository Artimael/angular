import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService} from '../../Service/service.service';
import { Usuario} from '../../Modelo/Usuario';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  usuario:Usuario=new Usuario();
  constructor(private service:ServiceService,private router:Router) { }

  ngOnInit(): void {
  }

  Guardar(){
    this.service.createUsuario(this.usuario)
    .subscribe(data=>{
      this.router.navigate(["listar"]);
    })
  }
}
