import {AbstractControl} from '@angular/forms';

function permittedValidator(control: AbstractControl):{[key :string ]:any} | null  {
	
	const permitted = /jazz/.test(control.value);
	
	return permitted ? {'permittedName': { value: control.value} } :null;
	
}