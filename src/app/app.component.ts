import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title='';
  description='';
  //constructor(private http: HttpClient){}
  
  constructor(private router:Router){}
  
  Listar(){
	  this.router.navigate(["listar"]);
  }
  
  Nuevo(){
	  this.router.navigate(["add"]);
  }
  
	  
  // ngOnInit(){
	  // this.http.get("http://localhost:8080/nombre",{responseType: 'text'}).subscribe(
	  // (resp:any) =>{
		// this.title=resp;
	  // })
	  
	   // this.http.get("http://localhost:8080/descripcion",{responseType: 'text'}).subscribe(
		  // (resp:any) =>{
		// this.description=resp;
	  // })
	  
  // }
  
  
}



